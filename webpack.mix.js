let mix = require('laravel-mix');
const webpack = require('webpack');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

mix.setPublicPath('public');
mix.setResourceRoot('../');
mix.extract(['jquery','popper.js','bootstrap','sweetalert2'],'public/js/vendor.js');

mix.autoload({
  jquery: ['jQuery', 'window.jQuery'],
   sweetalert:['swal']
});

mix.copyDirectory('assets/fonts/','public/fonts');
mix.sass('assets/css/core.scss','public/css/dashboard.css');
mix.js('assets/js/core.js','public/js/dashboard.js');
mix.js('assets/js/prolog.js','public/js/prolog.js');
mix.copyDirectory('assets/images','public/images');
