    :- use_module(library(lists)).
    :-dynamic(stageone/1).
    :-dynamic(stagetwo/1).
    :-dynamic(crisis/1).
    :-dynamic(elevated_level/1).
    :-dynamic(normal_level/2).
    :-dynamic(person/4).

    elevated_level(0).
    stageone(0).
    stagetwo(0).
    crisis(0).
    normal_level(0).
    person('Normal',0,0,'Normal').

    convert_weight(Pounds,Kilograms) :- Kilograms is Pounds/2.205.
    
    convert_height(Feet,Meters) :- Meters is Feet/39.37.
    
    calculate_bmi(Weight,Height,Risk_Level) :- convert_weight(Weight,Kilos),convert_height(Height,Meters),
    Bmi is (Kilos/(Meters*Meters)),riskCalc(Bmi,Risk_Level).

    riskCalc(Bmi, Risk_Level) :- Bmi < 18.5 -> Risk_Level = 'Underweight';
        (Bmi >= 18.5, Bmi =< 24.9) -> Risk_Level = 'Normal weight';
        (Bmi >= 25, Bmi =< 29.9) -> Risk_Level = 'Overweight';
        (Bmi >= 30) -> Risk_Level = 'Obesity'.

    has_hypertension(Level,LevelOfHypertension) :- (
    (Level == 2) -> (LevelOfHypertension ='Elevated',elevated_level(Elevated), NewElevated is Elevated + 1,retractall(elevated_level(_)),asserta(elevated_level(NewElevated)));
    (Level == 3)-> (LevelOfHypertension ='Stage One',stageone(StageOne),NewStageOne is StageOne + 1,retractall(stageone(_)),asserta(stageone(NewStageOne)));
    (Level == 4) -> (LevelOfHypertension ='Stage Two',stagetwo(StageTwo),NewStageTwo is StageTwo + 1,retractall(stagetwo(_)),asserta(stagetwo(NewStageTwo)));
    (Level == 5) -> (LevelOfHypertension ='Crisis. Please see a doctor',crisis(Crisis), NewCrisis is Crisis + 1,retractall(crisis(_)),asserta(crisis(NewCrisis)))
    ).

    blood_pressure(Bmi,Systolic, Diastolic,LevelOfHypertension) :- (
            (Systolic < 120, Diastolic < 80) -> Level is 1;
            ((Systolic >= 120, Systolic =< 129), (Diastolic < 80)) -> Level is 2;
            ((Systolic >=130 , Systolic =< 139); (Diastolic >= 80, Diastolic =< 89)) -> Level is 3;
            ((Systolic >=140 , Systolic =< 179 ); (Diastolic >=90, Diastolic =< 119)) -> Level is 4;
            ((Systolic >= 180, Diastolic >= 120)) -> Level is 5), has_hypertension(Level,LevelOfHypertension), add_person(Bmi,Systolic,Diastolic,LevelOfHypertension,_).

    add_person(Bmi,Systolic,Diastolic,LevelOfHypertension,_) :- asserta(person(Bmi,Systolic,Diastolic,LevelOfHypertension)).

    retrieve_number_of_crisis(_,Crisis) :- crisis(Crisis).
    retrieve_number_of_stage2(_,Stage2) :- stagetwo(Stage2).
    retrieve_number_of_stage1(_,Stage1) :- stageone(Stage1).
    retrieve_number_of_elevated(_,Elevated) :- elevated_level(Elevated).
    retrieve_number_of_elevated(_,Normal) :- normal_level(Normal).

    retrieve_numbers(Numbers) :- retrieve_number_of_elevated(_,Normal),retrieve_number_of_elevated(_,Elevated), retrieve_number_of_stage1(_,Stage1),
    retrieve_number_of_stage2(_,Stage2),retrieve_number_of_crisis(_,Crisis),Numbers = [Normal,Elevated,Stage1,Stage2,Crisis].
    retrieve_persons(Persons) :- findall([Bmi,Systolic,Diastolic,LevelOfHypertension],person(Bmi,Systolic,Diastolic,LevelOfHypertension),Persons).

