import * as pl from "tau-prolog";
import swal from 'sweetalert2';

window.swal = window.SweetAlert = swal;

let session = pl.create(1000);

let predicate = '    :- use_module(library(lists)).\n' +
    '    :-dynamic(stageone/1).\n' +
    '    :-dynamic(stagetwo/1).\n' +
    '    :-dynamic(crisis/1).\n' +
    '    :-dynamic(elevated_level/1).\n' +
    '    :-dynamic(normal_level/1).\n' +
    '    :-dynamic(person/4).\n' +
    '\n' +
    '    elevated_level(0).\n' +
    '    stageone(0).\n' +
    '    stagetwo(0).\n' +
    '    crisis(0).\n' +
    '    hypertension_level(0).\n' +
    '    person(\'Normal\',0,0,\'Normal\').\n' +
    '\n' +
    '    convert_weight(Pounds,Kilograms) :- Kilograms is Pounds/2.205.\n' +
    '\n' +
    '    convert_height(Feet,Meters) :- Meters is Feet/39.37.\n' +
    '\n' +
    '    calculate_bmi(Weight,Height,Risk_Level) :- convert_weight(Weight,Kilos),convert_height(Height,Meters),\n' +
    '    Bmi is (Kilos/(Meters*Meters)),riskCalc(Bmi,Risk_Level).\n' +
    '\n' +
    '    riskCalc(Bmi, Risk_Level) :- Bmi < 18.5 -> Risk_Level = \'Underweight\';\n' +
    '        (Bmi >= 18.5, Bmi =< 24.9) -> Risk_Level = \'Normal weight\';\n' +
    '        (Bmi >= 25, Bmi =< 29.9) -> Risk_Level = \'Overweight\';\n' +
    '        (Bmi >= 30) -> Risk_Level = \'Obesity\'.\n' +
    '\n' +
    '   has_hypertension(Level,LevelOfHypertension) :- (\n' +
    '    (Level == 1) -> (LevelOfHypertension =\'Normal\',normal_level(Normal), NewNormal is Normal + 1,retractall(noraml_level(_)),asserta(normal_level(NewNormal)));\n' +
    '    (Level == 2) -> (LevelOfHypertension =\'Elevated\',elevated_level(Elevated), NewElevated is Elevated + 1,retractall(elevated_level(_)),asserta(elevated_level(NewElevated)));\n' +
    '    (Level == 3)-> (LevelOfHypertension =\'Stage One\',stageone(StageOne),NewStageOne is StageOne + 1,retractall(stageone(_)),asserta(stageone(NewStageOne)));\n' +
    '    (Level == 4) -> (LevelOfHypertension =\'Stage Two\',stagetwo(StageTwo),NewStageTwo is StageTwo + 1,retractall(stagetwo(_)),asserta(stagetwo(NewStageTwo)));\n' +
    '    (Level == 5) -> (LevelOfHypertension =\'Crisis. Please see a doctor\',crisis(Crisis), NewCrisis is Crisis + 1,retractall(crisis(_)),asserta(crisis(NewCrisis)))\n' +
    '    ).\n' +
    '\n' +
    '    blood_pressure(Bmi,Systolic, Diastolic,LevelOfHypertension) :- (\n' +
    '        (Systolic < 120, Diastolic < 80) -> Level is 1;\n' +
    '        ((Systolic >= 120, Systolic =< 129), (Diastolic < 80)) -> Level is 2;\n' +
    '        ((Systolic >=130 , Systolic =< 139); (Diastolic >= 80, Diastolic =< 89)) -> Level is 3;\n' +
    '        ((Systolic >=140 , Systolic =< 179 ); (Diastolic >=90, Diastolic =< 119)) -> Level is 4;\n' +
    '        ((Systolic >= 180, Diastolic >= 120)) -> Level is 5), has_hypertension(Level,LevelOfHypertension), add_person(Bmi,Systolic,Diastolic,LevelOfHypertension,_).\n' +
    '\n' +
    '    add_person(Bmi,Systolic,Diastolic,LevelOfHypertension,_) :- asserta(person(Bmi,Systolic,Diastolic,LevelOfHypertension)).\n' +
    '\n' +
    '    retrieve_number_of_crisis(_,Crisis) :- crisis(Crisis).\n' +
    '    retrieve_number_of_stage2(_,Stage2) :- stagetwo(Stage2).\n' +
    '    retrieve_number_of_stage1(_,Stage1) :- stageone(Stage1).\n' +
    '    retrieve_number_of_elevated(_,Elevated) :- elevated_level(Elevated).\n' +
    '    retrieve_number_of_elevated(_,Normal) :- normal_level(Normal).\n' +
    '    \n' +
    '    retrieve_numbers(Numbers) :- retrieve_number_of_elevated(_,Normal),retrieve_number_of_elevated(_,Elevated), retrieve_number_of_stage1(_,Stage1),\n' +
    'retrieve_number_of_stage2(_,Stage2),retrieve_number_of_crisis(_,Crisis),Numbers = [Normal,Elevated,Stage1,Stage2,Crisis].\n' +
    '\n' +
    'retrieve_persons(Persons) :- findall([Bmi,Systolic,Diastolic,LevelOfHypertension],person(Bmi,Systolic,Diastolic,LevelOfHypertension),Persons).\n' +
    '\n' +
    '\n' +
    '    \n';


session.consult(predicate);

const expertSystems = {
    testHypertensive(weight,height,systolic,diastolic){
        session.query('calculate_bmi('+weight+','+height+',Risk).');
        session.answer(x => {
            let answer = pl.format_answer(x).split('=');
            answer = answer[1].split(";");
            let bmi = answer[0].replace(/[''']+/g,'');
            swal('What is your bmi level?',bmi,'info');
            console.log(bmi);
            if(bmi.localeCompare('Obesity')){
                console.log(session.query('blood_pressure('+bmi+','+systolic+','+ diastolic+',Hypertension).'));
                session.answer(level => console.log(pl.format_answer(level)));
            }
        });
    },
 reportHypertension(){
    let numbers = [];
    console.log(session.query('retrieve_numbers(Numbers).'));
    session.answer(x => {
        console.log(pl.format_answer(x));
        let answer = pl.format_answer(x).split('=');
        answer = answer[1].split(";");
        console.log(answer[0]);
        answer[0] = answer[0].replace('[','');
        answer[0] = answer[0].replace(']','');
        answer = answer[0].split(', ');
        for (let i = 0; i < answer.length; i++) {
            numbers.push(Number(answer[i]));
        }
    });
    return numbers;
},
    reportDetails(){
        let person = [];
        session.query('retrieve_persons(Persons).');
        session.answer(x => {
           let answer = pl.format_answer(x).split('=');
           answer = answer[1].split(';');
            for (let i = 0; i < answer.length; i++) {
                person.push(answer[i]);
            }
        });
        return person;

    }
};



export default expertSystems;
