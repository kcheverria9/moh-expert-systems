import $ from 'jquery';
import Vue from 'vue';
import VueRouter from 'vue-router';
import * as pl from 'tau-prolog';

import Dashboard from '../../components/Dashboard.vue';
import Reports from '../../components/Report.vue';
import Sidebar from '../../components/Sidebar.vue';
import NavigationBar from '../../components/NavigationBar.vue';
import NewPatient from '../../components/NewPatient';

require('popper.js');
require('bootstrap');

window.$ = window.jQuery = $;
window.pl = pl;

import owlCarousel from 'owl.carousel';
window.owlCarousel = owlCarousel;

import metisMenu from 'metismenu/dist/metisMenu';
window.metisMenu = metisMenu;
require('jquery-slimscroll');

import slicknav from 'slicknav/jquery.slicknav';
window.slicknav = slicknav;


Vue.use(VueRouter);

Vue.component('sidebar',Sidebar);

Vue.component('navigationbar',NavigationBar);

const routes = [
    { path: '/', component: Dashboard },
    { path: '/reports', component: Reports },
    { path: '/add-patient', component: NewPatient },

];

const router = new VueRouter({
    routes
});

new Vue({
    router
}).$mount('#app');

require('./plugins');
require('./scripts');
